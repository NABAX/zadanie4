package dz;

public class Circle extends Shape {

    private final Point center;
    private final double radius;

    public Circle ( Point center, double radius, Color color ) {
        super(color);
        this.center = center;
        this.radius = radius;

    }

    public Point getCenter() { return center; }

    public double getRadius() { return radius; }

    public double getArea() {
        return radius * radius * Math.PI;
    }

}