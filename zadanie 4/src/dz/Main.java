package dz;

public class Main {
    public static void main (String[] args) {

        Circle circle = new Circle(
                new Point(0, 0), 1, Color.WHITE);
        Triangle triangle = new Triangle(
                new Point(0,0), new Point(1,0), new Point(0,1), Color.BLUE);
        Square square = new Square(new Point(5,5), 2, Color.RED);

        Shape plTriangle = triangle;

        Shape plSquare = square;

        Shape plCircle = circle;

        System.out.println( "Площадь треугольника: " + plTriangle.getArea() );
        System.out.println( "Площадь квадрата: " + plSquare.getArea() );
        System.out.println( "Площадь треугольника: " + plCircle.getArea() );

    }



}
